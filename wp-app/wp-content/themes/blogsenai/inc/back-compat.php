<?php
/**
 * Blog SENAI back compat functionality
 *
 * Prevents Blog SENAI from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Blog SENAI 1.0.0
 */

/**
 * Prevent switching to Blog SENAI on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Blog SENAI 1.0.0
 */
function blogsenai_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'blogsenai_upgrade_notice' );
}
add_action( 'after_switch_theme', 'blogsenai_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Blog SENAI on WordPress versions prior to 4.7.
 *
 * @since Blog SENAI 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function blogsenai_upgrade_notice() {
	$message = sprintf( __( 'Blog SENAI requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'blogsenai' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since Blog SENAI 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function blogsenai_customize() {
	wp_die(
		sprintf(
			__( 'Blog SENAI requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'blogsenai' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'blogsenai_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since Blog SENAI 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function blogsenai_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Blog SENAI requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'blogsenai' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'blogsenai_preview' );
