<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php

		if ( have_posts() ) {
            get_template_part( 'template-parts/content/category-selector' );
            get_template_part( 'template-parts/content/sticky-posts' );
            ?>
            <div style="display: flex;margin: 20px calc(10% + 60px);">
                <div style="display: flex;width: 66%;flex-wrap: wrap;">
                <?php
                    get_template_part( 'template-parts/content/post-list' );
                ?>
                </div>
                <div style="display: flex; width: 33%;">
                    <?php
//                    $args = array(
//                        'header' => 'Popular',
//                        'header_start' => '<h3 class="title">',
//                        'header_end' => '</h3>'
//                    );
//                    wpp_get_mostpopular( $args );

                    $popular_posts_query = new WPP_Query( [] );
                    $popular_posts = $popular_posts_query->get_posts();
                    echo '<h5>Popular</h5><br/>';
                    foreach ( $popular_posts as $popular_post ) {
                        $post = get_post( $popular_post->id );
                        echo $post->post_title . '<br/>';
                    }
                    ?>
                </div>
            </div>
            <?php
			// Previous/next page navigation.
			blogsenai_the_posts_navigation();

		} else {

			// If no content, include the "No posts found" template.
			get_template_part( 'template-parts/content/content', 'none' );

		}
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
