<?php

$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$args = [
    'posts_per_page' => 2,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'paged' => $paged,
    'post_type' => 'post',
    'ignore_sticky_posts' => true
];
query_posts( $args );

while ( have_posts() ) {
    the_post();
    $categories = get_the_category();
    $category = $categories[0]; ?>
    <div style="width: 50%;">
        <?php
        echo get_the_post_thumbnail();
        echo get_the_title() . '<br/>';
        echo '<span style="padding: 10px; background-color:' . $category->description . '">' . $category->name . '</span><br/>';
        echo human_time_diff( get_the_time( 'G', $post ), time() ) . ' atrás • <br/>';
        echo $post->post_excerpt;
        ?><br/>
        <a href="<?php echo get_permalink(); ?>" style="color: #019685">
        <span style="display: flex;align-items: center;">
            Continuar lendo &nbsp;
            <?php echo blogsenai_get_icon_svg( 'arrow-right', 28 ); ?>
        </span>
        </a>
    </div>
    <?php
}
