<?php
/**
 * Template part for display sticky posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<?php
    $posts = blogsenai_get_sticky_posts();

    foreach ( $posts as $idx => $post ) { ?>
        <div id="slide-item<?php echo $idx;?>" style="<?php echo $idx == 0 ? 'display: flex;' : 'display: none;';?>margin: 0 calc(10% + 60px);">
            <div style="width: 50%;">
                <?php echo get_the_post_thumbnail( $post->ID ); ?>
            </div>
            <div style="width: 50%;">
                <h3><?php echo $post->post_title; ?></h3>
                <p>
                    <?php echo human_time_diff( get_the_time( 'G', $post ), time() ) . ' atrás •'; ?>
                    <?php echo do_shortcode( '
                        [rt_reading_time label="" postfix="min de leitura" postfix_singular="min de leitura" post_id="' . $post->ID . '"]'
                    ); ?>
                </p>
                <p><?php echo $post->post_excerpt; ?></p>
                <a href="<?php echo get_permalink(); ?>" style="color: #019685">
                    <span style="display: flex;align-items: center;font-weight: bold;">
                        Continuar lendo &nbsp;
                        <?php echo blogsenai_get_icon_svg( 'arrow-right', 28 );?>
                    </span>
                </a>
            </div>
        </div>
<?php
    }

// Controls
?>
<div style="display: flex;margin: 0 calc(10% + 60px);">
<?php
    foreach ( $posts as $idx => $post ) { ?>
        <div style="width: 25%; margin: 4px;padding: 16px; background-color: #f0f5f8;"
            onclick="changeSlideItem('<?php echo $idx?>')">
            <?php echo $post->post_title; ?>
        </div>
<?php
    }
?>
</div>
<script>
function changeSlideItem(item) {
    var slideItems = document.querySelectorAll('div[id^=slide-item]');
    slideItems.forEach(function(node) {
        node.style.display = 'none';
    });
    var slideItem = document.querySelector('#slide-item'+item);
    slideItem.style.display = 'flex';
}
</script>
