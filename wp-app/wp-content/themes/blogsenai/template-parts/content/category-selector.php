<?php
    $categories = get_categories();
if ( count( $categories ) ) {
    ?>
    <div class="category-filter-container">
        <?php
        foreach ( $categories as $category ) { ?>
            <a href="<?php echo get_category_link( $category ); ?>">
                <div class="category-option" style="color:<?php echo $category->description ? $category->description : '#000;'; ?>">
                    <?php echo $category->name; ?>
                </div>
            </a>
            <?php
        }
        ?>
    </div>
    <?php
}
