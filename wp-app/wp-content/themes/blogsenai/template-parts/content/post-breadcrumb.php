<div class="post-breadcrumb">
    <?php
    $category = get_the_category();
    ?>
    <a href="<?php echo site_url(); ?>">Início</a>
     >
    <a href="<?php echo get_category_link( $category[0] ); ?>"><?php echo $category[0]->name;?></a>
     >
    <?php echo the_title(); ?>
</div>
