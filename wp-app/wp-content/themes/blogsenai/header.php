<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'blogsenai' ); ?></a>

		<header id="masthead" class="site-header">

			<div class="site-branding-container">
				<?php //get_template_part( 'template-parts/header/site', 'branding' ); ?>
                <a class="brand-link" target="_blank" href="http://fiesc.com.br">FIESC</a>
                <a class="brand-link" target="_blank" href="http://ciesc.com.br">CIESC</a>
                <a class="brand-link" target="_blank" href="http://sesisc.org.br">SESI</a>
                <a class="brand-link" target="_blank" href="http://sc.senai.br">SENAI</a>
                <a class="brand-link" target="_blank" href="http://ielsc.org.br">IEL</a>
                <div class="header-right">
                    <a href="#">Cursos SENAI</a>
                    <a href="#">Entre em Contato</a>
                </div>
			</div><!-- .site-branding-container -->

            <div class="site-branding-subheader-container">
                <div class="content">
                    <div class="icon-home">
                        <a href="/">
                            <img src="<?php bloginfo('template_url'); ?>/images/logo-sesi-senai.png" />
                        </a>
                    </div>
                    <?php blogsenai_get_search_form(); ?>
                </div>
            </div>

		</header><!-- #masthead -->

	<div id="content" class="site-content">
